Beamline 8.3.2: MicroCT at the ALS
==================================

About BL 8.3.2
--------------

Welcome to the Python bitbucket page for beamline 8.3.2 at the Advanced Light Source. Beamline 8.3.2 is a synchrotron-based
hard X-ray Micro-Tomography instrument. It allows non-destructive 3-dimensional imaging of solid objects at a resolution
of approximately 1 micron. See our website_ here.

.. _website: http://microct.lbl.gov/

This repo contains Python tools for processing BL 8.3.2. For a repo containing ImageJ tools, see this_ repo.

.. _this: https://bitbucket.org/berkeleylab/als-microct-imagej/


Installing Xi-cam
-----------------

We highly recommend using either Linux or Mac if you would like to install Xi-cam, as tomopy_ is not supported on
Windows.

.. _tomopy: https://tomopy.readthedocs.io/en/latest/

Linux
+++++

**This installation requires conda for Python 2.7.** If you do not have conda, please download it first here_.

.. _here: https://conda.io/miniconda.html

Download the ``xi-cam_env.yml`` file from this repo. In a terminal, go to the directory where
the yml file was downloaded. For example, if it was downloaded to '/home/alsmicroct/Downloads/xi-cam_env.yml', type

.. code-block:: bash

 cd /home/alsmicroct/Downloads/

Then run the following commands:

.. code-block:: bash

 conda env create -f xi-cam_env.yml
 source activate xi-cam

This will activate the conda environment. Typing ``xicam`` into the terminal with the activated environment should run
Xi-cam. To deactivate the environment, enter ``source deactivate`` in the terminal.

One problem you may run into is that the PySide version prescribed in the xi-cam_env.yml file may not work for your
operating system. The following systems should use the corresponding PySide version:

*   PySide 1.2.0: CentOS 7; Ubuntu 14, 16
*   Pyside 1.2.1: openSUSE

The default PySide version is 1.2.0. To install 1.2.1, run the following command with the conda environment active:

.. code-block:: bash

 conda install pyside=1.2.1

You can also install 1.2.4 by running the following command:

.. code-block:: bash

 conda install -c conda-forge pyside=1.2.4

PySide 1.2.2 is not available on conda but is available via pip_.

.. _pip: https://pypi.python.org/pypi/PySide/1.2.2

If you don't see your operating system listed or if the recommended version does not work for your system, please try each version listed to see which works and email the result to hparks@lbl.gov so I can add it to the above list.