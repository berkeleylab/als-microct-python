# -*- coding: utf-8 -*-
"""
heartbeat_bl832 script used to test that the full data processing pipeline
starting from the spot832 computer at beamline 8.3.2 all the way through
reconstruction of data and uploading of raw/normalizations/sinograms/recons to
SPOT website.

The script requires an input h5 file with the same file metadata as a regular
bl832 h5 file, one h5 group within the file containing the same metadata as a
regular bl832 h5 file and a 3D tomography dataset within the group containing
the metadata that a single projection dataset has in a regular bl832 h5 file.

The default input hdf5 file 'input_heartbeat_file.h5' contains low resolution
tomography data of lego man. (64 59x50 projections).

Apart from meeting the above h5 structure criteria, it is recommended that if
another input h5 file is to be used instead of the original low resolution
data with a small number of projections is used.

@author: lbluque
"""

import sys
import os
import argparse
import h5py
import smtplib
import logging
from uuid import uuid1
from email.mime.text import MIMEText
from numpy import zeros, ones, copy
from datetime import datetime

SENDER = 'wizard@spot832.lbl.gov'
RECIPIENT_LIST = ['spot-notify@lbl.gov']


def send_email(sender, recipients, subject, text):

    msg = MIMEText(text)
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(recipients)

    try:
        s = smtplib.SMTP('localhost')
        s.sendmail(sender, recipients, msg.as_string())
        s.quit()
        logger.info('Notification email sent')
    except Exception:
        logger.error('Failed to send notification email', exc_info=True)
        raise


def read_input_h5(fname):

    try:
        with h5py.File(fname, 'r') as f:
            file_metadata = f.attrs.items()
            group = f.items()
            if len(group) == 1 and isinstance(group[0][1], h5py.Group):
                group_metadata = group[0][1].attrs.items()
                dataset = group[0][1].items()
                if len(dataset) == 1 and isinstance(dataset[0][1],
                                                    h5py.Dataset):
                    dataset_metadata = dataset[0][1].attrs.items()
                    data = copy(dataset[0][1])
                else:
                    raise RuntimeError
            else:
                raise RuntimeError
    except Exception:
        logger.error('Failed to read input h5 file', exc_info=True)
        raise

    return file_metadata, group_metadata, dataset_metadata, data


def write_experiment_data(h5obj, metadata, dname, proj,
                          sino, rays, args, time):

    for i in range(len(metadata)):
        if metadata[i][0] == 'object':
            h5obj.attrs.create(metadata[i][0], dname + '\r')
        elif metadata[i][0] == 'sdate':
            h5obj.attrs.create(metadata[i][0],
                               time.strftime('%m-%d-%Y %H:%M:%S\r'))
        elif metadata[i][0] == 'experimenter':
            h5obj.attrs.create(metadata[i][0], args.owner + '\r')
        elif metadata[i][0] == 'nangles':
            h5obj.attrs.create(metadata[i][0], str(proj) + '\r')
        elif metadata[i][0] in ('nslices',
                                'dzelements',
                                'rzelements'):
            h5obj.attrs.create(metadata[i][0], str(sino) + '\r')
        elif metadata[i][0] in ('nrays',
                                'dxelements',
                                'rxelements',
                                'ryelements'):
            h5obj.attrs.create(metadata[i][0], str(rays) + '\r')
        elif metadata[i][0] == 'num_dark_fields':
            h5obj.attrs.create(metadata[i][0],
                               str(args.num_dark) + '\r')
        elif metadata[i][0] == 'num_bright_field':
            h5obj.attrs.create(metadata[i][0],
                               str(args.num_bright) + '\r')
        elif metadata[i][0] == 'time_stamp':
            h5obj.attrs.create(metadata[i][0],
                               time.strftime('%m/%d/%Y %H:%M%S PM\r'))
        else:
            h5obj.attrs.create(metadata[i][0], metadata[i][1])


def write_heartbeat_h5(full_path, data, file_metadata, group_metadata,
                       dataset_metadata, dataset_name, args, time):

    proj, sino, rays = data.shape

    try:
        with h5py.File(full_path, 'w') as f:
            for i in range(len(file_metadata)):
                if file_metadata[i][0] == 'owner':
                    f.attrs.create(file_metadata[i][0], args.owner)
                elif file_metadata[i][0] == 'stage_date':
                    f.attrs.create(file_metadata[i][0],
                                   time.strftime('%Y-%m-%dT%H:%M:%S.000+0000'))
                elif file_metadata[i][0] == 'dataset':
                    f.attrs.create(file_metadata[i][0], dataset_name)
                elif file_metadata[i][0] == 'uuid':
                    uuid = str(uuid1())
                    f.attrs.create(file_metadata[i][0], uuid)
                else:
                    f.attrs.create(file_metadata[i][0], file_metadata[i][1])

            if args.tile == 1:
                f.create_group(dataset_name)
                g = f[dataset_name]
            else:
                useless_group = dataset_name[:-7]
                f.create_group(useless_group)
                f[useless_group].create_group(dataset_name)
                g = f[useless_group][dataset_name]

            write_experiment_data(g, group_metadata, dataset_name, proj,
                                  sino, rays, args, time)

            for i in range(proj):
                dataset = (dataset_name + '_0000_{0:0={1}d}'.format(i, 4) +
                           '.tif')
                image = zeros((1, sino, rays), dtype=int)
                image[0] = data[i]
                g.create_dataset(dataset, dtype='uint16', data=image)
                d = g[dataset]
                write_experiment_data(d, group_metadata, dataset_name, proj,
                                      sino, rays, args, time)
                for j in range(len(dataset_metadata)):
                    if dataset_metadata[j][0] == 'dim2':
                        d.attrs.create(dataset_metadata[j][0], str(sino))
                    elif dataset_metadata[j][0] == 'dim3':
                        d.attrs.create(dataset_metadata[j][0], str(rays))
                    elif dataset_metadata[j][0] == 'date':
                        d.attrs.create(dataset_metadata[j][0],
                                       time.strftime('%Y-%m-%dT%H:%MZ '))
                    else:
                        d.attrs.create(dataset_metadata[j][0],
                                       dataset_metadata[j][1])
            last = proj - 1
            for i in range(args.num_bright):
                dataset = (dataset_name + 'bak_{0:0={1}d}_0000'.format(i, 4) +
                           '.tif')
                image = 32768 * ones((1, sino, rays), dtype=int)
                g.create_dataset(dataset, dtype='uint16', data=image)
                d = g[dataset]
                for j in range(len(dataset_metadata)):
                    if dataset_metadata[j][0] == 'dim2':
                        d.attrs.create(dataset_metadata[j][0], str(sino))
                    elif dataset_metadata[j][0] == 'dim3':
                        d.attrs.create(dataset_metadata[j][0], str(rays))
                    elif dataset_metadata[j][0] == 'date':
                        d.attrs.create(dataset_metadata[j][0],
                                       time.strftime('%Y-%m-%dT%H:%MZ '))
                    else:
                        d.attrs.create(dataset_metadata[j][0],
                                       dataset_metadata[j][1])

                dataset = (dataset_name +
                           'bak_{0:0={2}d}_{1:0={2}d}'.format(i, last, 4) +
                           '.tif')
                image = 32768 * ones((1, sino, rays), dtype=int)
                g.create_dataset(dataset, dtype='uint16', data=image)
                d = g[dataset]
                for j in range(len(dataset_metadata)):
                    if dataset_metadata[j][0] == 'dim2':
                        d.attrs.create(dataset_metadata[j][0], str(sino))
                    elif dataset_metadata[j][0] == 'dim3':
                        d.attrs.create(dataset_metadata[j][0], str(rays))
                    elif dataset_metadata[j][0] == 'date':
                        d.attrs.create(dataset_metadata[j][0],
                                       time.strftime('%Y-%m-%dT%H:%MZ '))
                    else:
                        d.attrs.create(dataset_metadata[j][0],
                                       dataset_metadata[j][1])

            for i in range(args.num_dark):
                dataset = (dataset_name +
                           'drk_{0:0={2}d}_{1:0={2}d}'.format(i, last, 4) +
                           '.tif')
                image = zeros((1, sino, rays), dtype=int)
                g.create_dataset(dataset, dtype='uint16', data=image)
                d = g[dataset]
                for j in range(len(dataset_metadata)):
                    if dataset_metadata[j][0] == 'dim2':
                        d.attrs.create(dataset_metadata[j][0], str(sino))
                    elif dataset_metadata[j][0] == 'dim3':
                        d.attrs.create(dataset_metadata[j][0], str(rays))
                    elif dataset_metadata[j][0] == 'date':
                        d.attrs.create(dataset_metadata[j][0],
                                       time.strftime('%Y-%m-%dT%H:%MZ '))
                    else:
                        d.attrs.create(dataset_metadata[j][0],
                                       dataset_metadata[j][1])

        os.chmod(full_path, 0o770)

    except Exception:
        logger.error('Failed to write heartbeat h5 file', exc_info=True)
        raise


def heartbeat(argv):

    # Read and parse system arguments
    parser = argparse.ArgumentParser(description='create a test HDF5 file in '
                                     'current BL832 format')
    parser.add_argument('-i', '--input', type=str, help='input hdf5 file',
                        default='input_heartbeat_file.h5')
    parser.add_argument('-b', '--num_bright', type=int, help='number of test '
                        'bright fields', default=6)
    parser.add_argument('-d', '--num_dark', type=int, help='number of test '
                        'dark fields', default=4)
    parser.add_argument('-u', '--owner', type=str, help='user/owner name',
                        default='dyparkinson')
    parser.add_argument('-a', '--base-name', type=str, help='Base name of test'
                        ' file', default='bl832_heartbeat')
    parser.add_argument('-o', '--out-dir', type=str, help='output directory '
                        'where test file will be saved', default=os.getcwd())
                        # default=os.path.join('/global', 'raw', 'testuser'))
    parser.add_argument('-t', '--tile', type=int,
                        help='Number of tiled test datasets', default=1)

    args = parser.parse_args()
    logger.debug('Parsed arguments: %s', args)

    # Read input hdf5 file data and metadata
    file_metadata, group_metadata, dataset_metadata, data = read_input_h5(args.input)
    logger.info('Input hdf5 file read succesfully')

    # Create time object, set time_stamp and file name; make output file dir
    time = datetime.now()
    time_stamp = time.strftime('%Y%m%d_%H%M%S_')
    stamped_name = time_stamp + args.base_name
    new_dir = args.out_dir + '/' + stamped_name
    os.mkdir(new_dir)
    chunk = (data.shape[1] + 1)//args.tile

    for i in range(args.tile):

        if args.tile == 1:
            dataset_name = stamped_name
            full_path = os.path.join(new_dir, dataset_name + '.h5')
        else:
            dataset_name = stamped_name + '_x00y{0:0={1}d}'.format(i, 2)
            os.mkdir(os.path.join(new_dir, dataset_name))
            full_path = os.path.join(new_dir, dataset_name, dataset_name + '.h5')

        logger.info('New directory (%s) was created.', new_dir)

        # data indices
        start = i*chunk
        end = (i + 1)*chunk

        # Create output hdf5 file with bl832 format
        write_heartbeat_h5(full_path, data[:, start:end, :], file_metadata,
                           group_metadata, dataset_metadata, dataset_name,
                           args, time)
        logger.info('Heartbeat h5 file %s written!', full_path)

        # Write .done file
        done_filename = dataset_name + '.done'
        done_file = os.path.join(new_dir, done_filename)
        with open(done_file, 'w') as f:
            f.write('0')

        os.chmod(done_file, 0o770)
        logger.info('.done file written to %s', done_file)

    # Write done.txt as tmp file
# NO LONGER USING DONE.TXT FILES
#    tmp = os.path.join(new_dir, 'tmp')
#    with open(tmp, 'w') as f:
#            f.write('0')
#    os.chmod(tmp, 0o770)
#    logger.info('tmp (done.txt) file written to %s', tmp)
#
#    # Move tmp to done.txt file
#    mv_done_file = os.path.join(new_dir, 'done.txt')
#    os.rename(tmp, mv_done_file)
#    logger.info('moved tmp file to done.txt')

    # Send notification email
    date_and_time = time.strftime('%H:%M:%S on %m/%d/%Y')
    text = str('Heartbeat test file ' + stamped_name + ' was written to ' +
               'spot832 at ' + date_and_time + '.\n')

    subject = str('Heartbeat test file ' + dataset_name + '.h5 written!')

    send_email(SENDER, RECIPIENT_LIST, subject, text)
    logger.info('Notification email sent!')
    logger.info('%s', text)


if __name__ == "__main__":
    loghandler = logging.FileHandler('bl832_heartbeat.log', 'a')
    formatter = logging.Formatter('%(asctime)s-%(levelname)s : %(message)s')
    loghandler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(loghandler)
    logger.setLevel(logging.INFO)
    heartbeat(sys.argv[1:])
